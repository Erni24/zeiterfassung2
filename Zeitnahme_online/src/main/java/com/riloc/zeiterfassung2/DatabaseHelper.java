package com.riloc.zeiterfassung2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class DatabaseHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = DatabaseHelper.class.getSimpleName();

    final private String event;

    // Database Name
    private static final String DATABASE_NAME = "_time_db";
    private static final int DB_VERSION = 1;

    public DatabaseHelper(Context context, String event) {
        super(context, event+DATABASE_NAME, null, DB_VERSION);
        this.event = event;
        Log.d(LOG_TAG, "DbHelper hat die Datenbank: " + getDatabaseName() + " erzeugt.");
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        //just called on object creation if database file not exists
        try {
            Log.d(LOG_TAG, "Die Tabelle wird mit SQL-Befehl angelegt.");
            // create table
            db.execSQL("CREATE TABLE ergebnisse (start_nr TEXT DEFAULT '0' PRIMARY KEY, zeit1 time NOT NULL DEFAULT '00:00:00', " +
                    "zeit2 time NOT NULL DEFAULT '00:00:00', zeit3 time NOT NULL DEFAULT '00:00:00', zeit4 time NOT NULL DEFAULT '00:00:00', " +
                    "zeit5 time NOT NULL DEFAULT '00:00:00', zeit6 time NOT NULL DEFAULT '00:00:00', zeit7 time NOT NULL DEFAULT '00:00:00', sent int NOT NULL DEFAULT 0)");
            db.execSQL("CREATE TABLE result_not_assigned (start_nr TEXT DEFAULT '0' PRIMARY KEY, zeit1 time NOT NULL DEFAULT '00:00:00', " +
                    "zeit2 time NOT NULL DEFAULT '00:00:00', zeit3 time NOT NULL DEFAULT '00:00:00', zeit4 time NOT NULL DEFAULT '00:00:00', " +
                    "zeit5 time NOT NULL DEFAULT '00:00:00', zeit6 time NOT NULL DEFAULT '00:00:00', zeit7 time NOT NULL DEFAULT '00:00:00', sent int NOT NULL DEFAULT 0)");
        }catch(Exception ex){
            Log.e(LOG_TAG, "Fehler beim Anlegen der Tabelle: " + ex.getMessage());
        }
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + event + "`_ergebnisse` ");

        // Create tables again
        onCreate(db);
    }

    public void onUpdate(String stnr, String time){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Time_Singleton.getInstance().getSValue("event_time_position"), time);

        long id = db.insert("ergebnisse", null,  values);//, "start_nr = ?",
          //      new String[]{String.valueOf(stnr)});
        Log.i(LOG_TAG, "onUpdate: " + String.valueOf(id));
    }
}
