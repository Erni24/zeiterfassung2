package com.riloc.zeiterfassung2;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final int GPS_REQUEST = 1;
    private static final int TIME_VALUE = 2;
    private static final String TAG = Activity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1234);
        }

        initSingletons();
        Time_Singleton.getInstance().readSettings(this);
    }

    private void initSingletons() {
        // Initialize the instance of MySingleton
        //Time_Singleton.initInstance(getApplicationContext());
    }

    /**
     * Called when the user taps the gps button
     */
    public void getDBConnection(View view) {
        // Start the SecondActivity
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, GPS_REQUEST);

    }

    /**
     * Called when the user taps the time button
     */
    public void callTiming(View view) {

        if (!Time_Singleton.getInstance().getSValue("event_time_position").equals("")) {

            Intent intent;
            intent = new Intent(this, Zeit_Erfassung.class);
            //intent.putExtra(EXTRA_MESSAGE, message);

            startActivityForResult(intent, TIME_VALUE);
        }else{
            Toast.makeText(this.getBaseContext(), getResources().getString(R.string.no_position_set), Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Called when the user taps the time button
     */
    public void callTiming2(View view) {

        if (!Time_Singleton.getInstance().getSValue("event_time_position").equals("")) {

            Intent intent;
            intent = new Intent(this, Timing.class);
            //intent.putExtra(EXTRA_MESSAGE, message);

            startActivityForResult(intent, TIME_VALUE);
        }else{
            Toast.makeText(this.getBaseContext(), getResources().getString(R.string.no_position_set), Toast.LENGTH_LONG).show();
        }

    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == GPS_REQUEST) {
            Time_Singleton.getInstance().writeSettings(this);
        } else if (requestCode == TIME_VALUE) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: ");
            }
        }
    }
}
