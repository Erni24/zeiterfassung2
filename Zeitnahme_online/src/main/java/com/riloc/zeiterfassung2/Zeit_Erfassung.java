package com.riloc.zeiterfassung2;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Zeit_Erfassung extends Activity {

    private static final String TAG = Activity.class.getSimpleName();

    private ArrayList<Time_Table_Entry> liste;
    private Time_Table_Adapter time_list_adapter;
    private int lastInsertId = 0;
    private boolean databaseIsOpen = false;
    private TimeDataSource dataSource = null;
    private Handler handlerDataSend;
    private Runnable runnableDataSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context mContext;
        mContext = this.getBaseContext();
        setContentView(R.layout.activity_zeit_erfassung);

        dataSource = new TimeDataSource(mContext);

        TextView text = findViewById(R.id.text_position_for_time);
        text.setText(getResources().getString(R.string.position, Time_Singleton.getInstance().getSValue("event_time_position")));

        Log.d(TAG, "onCreate: " + dataSource.getDatabaseName());

        final ListView mTimeListView = findViewById(R.id.timeListView);

        mTimeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int position = i;
                final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                final AlertDialog dialog;

                builder.setTitle(R.string.get_start_nubmer_dialog_title)
                        .setMessage(getResources().getString(R.string.click_save_changes))
                        .setCancelable(false)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //cancel the dialog box
                                dialog.cancel();
                            }
                        });

                LinearLayout layout = new LinearLayout(builder.getContext());
                layout.setOrientation(LinearLayout.VERTICAL);
                final EditText text = new EditText(builder.getContext());
                text.setInputType(InputType.TYPE_CLASS_NUMBER);
                text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                text.setText(liste.get(position).getStnr());
                layout.addView(text);

                Button button = new Button(builder.getContext());
                button.setText(R.string.save_edit_to_startnummer);
                layout.addView(button);

                builder.setView(layout);

                dialog = builder.create();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        // hide soft keyboard
                        InputMethodManager imm = (InputMethodManager) dialog.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        //imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                        mTimeListView.invalidateViews();
                    }
                });
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //display in long period of time
                        liste.get(position).setStnr(text.getText().toString());

                        long id = dataSource.update(liste.get(position));

                        Toast.makeText(builder.getContext(), "alte StNr: " + liste.get(position).getOldStnr() + " neue StNr:" + liste.get(position).getStnr() + "\n" +
                                getResources().getString(R.string.save_to_sqlite_ready), Toast.LENGTH_LONG).show();

                        if (id > 0) {
                            dataSource.deleteFromNotAssignedDb(liste.get(position).getOldStnr());
                            liste.get(position).setOldStnr(liste.get(position).getStnr());
                        }

                        dialog.cancel();
                    }
                });

                dialog.show();
                text.requestFocus();
                text.selectAll();

                // show soft keyboard
                InputMethodManager imm = (InputMethodManager) dialog.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        });

        liste = new ArrayList<>();
        time_list_adapter = new Time_Table_Adapter(this, liste);

        mTimeListView.setAdapter(time_list_adapter);

        /*
        handler controls the sending of the data to sql-server
         */
        handlerDataSend = new Handler();
        runnableDataSend = new Runnable() {
            @Override
            public void run() {
                sendSQL(false);
                /*
                send data to SQL server raw data table every 30 seconds
                 */
                handlerDataSend.postDelayed(runnableDataSend, 30000);
                Log.d(TAG, "runnableDataSend run: done.");
            }
        };
        handlerDataSend.post(runnableDataSend);
/*
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
        */
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataSource.open()) {
            databaseIsOpen = true;
            lastInsertId = dataSource.getLastInsertId();

            Cursor cursor = dataSource.selectNotAssigned();

            if (cursor.moveToFirst()) {
                time_list_adapter.clear();
                while (!cursor.isAfterLast()) {
                    Log.d(TAG, "onCreate: " + cursor.getString(1));
                    Time_Table_Entry entry = new Time_Table_Entry(cursor.getString(0), cursor.getString(1));
                    dataSource.updateNotAssigned(entry);
                    time_list_adapter.add(entry);
                    cursor.moveToNext();
                }
            }
            handlerDataSend.post(runnableDataSend);
        }
    }

    @Override
    protected void onPause() {
        dataSource.close();
        databaseIsOpen = false;
        handlerDataSend.removeCallbacks(runnableDataSend);
        super.onPause();
    }
/*
    public void return_to_main_zeit(View view) {
        // put the String to pass back into an Intent and close this activity
        Intent intent = new Intent();
        intent.putExtra("keyName", "Test2");
        setResult(RESULT_OK, intent);
        finish();
    }

    public void getSQL(View view) {

        String url = Time_Singleton.getInstance().getSValue("db_url");
        url = (url.charAt(url.length() - 1) == '/' ? url.substring(0, url.length() - 1) : url);
        url += "/get_data.php";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        // Result handling
                        System.out.println(response);

                        // set stnr view with string

                        try {
                            JSONArray jsonResponse = new JSONArray(response);
                            for (int i = 0; i < jsonResponse.length(); i++) {
                                JSONObject jsonobject = jsonResponse.getJSONObject(i);
                                String name = jsonobject.getString("name");
                                String url = jsonobject.getString("ort");
                            }
                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse: " + e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // Error handling
                System.out.println("Something went wrong!");
                error.printStackTrace();

            }
        });

        // Add the request to the queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
*/
    public void clickSendSQL(View view) {
        sendSQL(true);
    }

    private void sendSQL(boolean triggeredByClick) {

        String url = Time_Singleton.getInstance().getSValue("db_url");
        url = (url.charAt(url.length() - 1) == '/' ? url.substring(0, url.length() - 1) : url);

        url += "/set_data.php";
        //String url = "http://192.168.42.100/set_data.php";

        //TimeDataSource dataSource = new TimeDataSource(this);
        //dataSource.open();

        final Cursor cursor = dataSource.select();

        Log.d(TAG, "sendSQL: " + cursor.getCount());
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            final JSONArray jsonSend = new JSONArray();

            try {

                JSONObject jsonObj = new JSONObject();
                jsonObj.put("id", 0);
                jsonObj.put("task", "getPositions");
                jsonSend.put(jsonObj);
                jsonObj = new JSONObject();
                jsonObj.put("id", 1);
                jsonObj.put("db_name", Time_Singleton.getInstance().getSValue("db_name"));
                jsonSend.put(jsonObj);
                jsonObj = new JSONObject();
                jsonObj.put("id", 2);
                jsonObj.put("db_user", Time_Singleton.getInstance().getSValue("db_user"));
                jsonSend.put(jsonObj);
                jsonObj = new JSONObject();
                jsonObj.put("id", 3);
                jsonObj.put("db_pass", Time_Singleton.getInstance().getSValue("db_pass"));
                jsonSend.put(jsonObj);
                jsonObj = new JSONObject();
                jsonObj.put("id", 4);
                jsonObj.put("db_event", Time_Singleton.getInstance().getSValue("db_event"));
                jsonSend.put(jsonObj);

                while (!cursor.isAfterLast()) {
                    jsonObj = new JSONObject();
                    jsonObj.put("start_nr", cursor.getString(0));
                    jsonObj.put(Time_Singleton.getInstance().getSValue("event_time_position"), cursor.getString(1));
                    jsonSend.put(jsonObj);
                    cursor.moveToNext();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.POST, url,
                    jsonSend,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.i("LO_VOLLEY_Response: ", response.toString() + " rows inserted");
                            try {
                                for (int i = 1; i < response.length(); ++i) {
                                    dataSource.updateSetSent(response.getString(i));
                                }
                                String text = ("Erfolg: ").concat(response.getString(0)).concat(" Eintrag/Einträge gespeichert");
                                Toast.makeText(Zeit_Erfassung.this, text, Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                Log.i("JSON DB sent Events", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("LOG_VOLLEY_error: ", error.toString());
                    Toast.makeText(Zeit_Erfassung.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            }) {

            };

            // Add the request to the queue
            Volley.newRequestQueue(this).add(arrayRequest);
        }else if (triggeredByClick){
            Toast.makeText(Zeit_Erfassung.this, getResources().getString(R.string.no_record_submitable), Toast.LENGTH_SHORT).show();
        }
    }

    public void addEntry(View view) {
        lastInsertId++;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SS", Locale.getDefault());
        Date system_time = new Date(System.currentTimeMillis() + Time_Singleton.getInstance().getLValue("difference"));

        Time_Table_Entry entry = new Time_Table_Entry(String.format(Locale.getDefault(), "x%d", lastInsertId), format.format(system_time));
        dataSource.updateNotAssigned(entry);
        time_list_adapter.add(entry);
    }
/*
    public void saveToSQLite(View view) {
        TimeDataSource dataSource = new TimeDataSource(this);

        Log.d(TAG, "Die Datenquelle wird geöffnet.");
        dataSource.open();

        for (int lo_int = 0; lo_int < time_list_adapter.getCount(); ++lo_int) {
            Time_Table_Entry item = time_list_adapter.getItem(lo_int);

            /*
                if the start number begins with a 'x', the entry is stored in a garbage database for reloading
                at open up the dialog again
             */
/*
            if (item.getStnr().charAt(0) == 'x') {
                dataSource.updateNotAssigned(item);
            } else {
                dataSource.update(item);
            }
        }

        Log.d(TAG, "Die Datenquelle wird geschlossen.");
        dataSource.close();

        Toast.makeText(view.getContext(), getResources().getString(R.string.save_to_sqlite_many_ready), Toast.LENGTH_LONG).show();
    }
*/
}
