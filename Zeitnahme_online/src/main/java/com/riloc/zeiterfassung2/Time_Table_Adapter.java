package com.riloc.zeiterfassung2;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

class  Time_Table_Adapter extends ArrayAdapter<Time_Table_Entry>{

    private static final String TAG = Activity.class.getSimpleName();

    private final List<Time_Table_Entry> list;
    private final Activity context;
    private int listPosition;

    public Time_Table_Adapter(Activity context, List<Time_Table_Entry> entries){
        super (context, 0, entries);
        this.context = context;
        this.list = entries;
    }

    static class ViewHolder{
        private long rowid;
        private boolean erg_table;
        private String old_st_nr;
        private TextView stnr;
        private TextView time;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        listPosition = position;
        ViewHolder viewHolder;

        // Get the data item for this position
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.time_table_item, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.stnr = convertView.findViewById(R.id.startnummerText);
            viewHolder.time = convertView.findViewById(R.id.timeTextView);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.startnummerText, viewHolder.stnr);
            convertView.setTag(R.id.timeTextView, viewHolder.time);

        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.rowid = list.get(position).getRowId();
        viewHolder.erg_table = list.get(position).getErgTable();
        viewHolder.old_st_nr = list.get(position).getOldStnr();
        viewHolder.stnr.setText(list.get(position).getStnr());
        viewHolder.time.setText(list.get(position).getCrossing_line());

        if (viewHolder.old_st_nr.charAt(0) == 'x')
            convertView.setBackgroundColor( Color.argb ( 100, 255, 100, 100 ) );
        else
            convertView.setBackgroundColor( Color.argb ( 100, 100, 255, 100 ) );

        return convertView;
    }
}
