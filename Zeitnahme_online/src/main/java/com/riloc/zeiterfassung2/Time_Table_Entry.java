package com.riloc.zeiterfassung2;

class Time_Table_Entry {
    private long rowid;
    private boolean erg_table;
    private String old_start_number;
    private String start_number;
    private String crossing_line;

    public Time_Table_Entry(String stnr, String time){
        this.rowid = -1;
        this.erg_table = false;
        this.start_number = stnr;
        this.old_start_number = stnr;
        this.crossing_line = time;
    }

    public long getRowId(){
        return rowid;
    }

    public void setRowId(long value){
        rowid = value;
    }

    public boolean getErgTable(){
        return erg_table;
    }

    public void setErgTable(boolean value){
        erg_table = value;
    }

    public String getOldStnr(){
        return old_start_number;
    }

    public void setOldStnr(String value){
        old_start_number = value;
    }

    public String getStnr(){
        return start_number;
    }

    public void setStnr(String value){
        start_number = value;
    }

    public String getCrossing_line(){ return crossing_line; }

    public void setCrossing_line(String value){
        crossing_line = value;
    }
}
