package com.riloc.zeiterfassung2;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Formatter;
import java.util.Locale;

class Time_Singleton {
    private static Time_Singleton ourInstance;

    private long lo_difference;

    private String lo_url_text;
    private String lo_db_name_text;
    private String lo_db_user_text;
    private String lo_db_pass_text;
    private String lo_db_event_text;
    private String lo_time_position_text;

    static Time_Singleton getInstance() {
        if (ourInstance == null) {
            ourInstance = new Time_Singleton();
        }
        return ourInstance;
    }

    private Time_Singleton() {
        lo_difference = 0;

        lo_url_text = "";
        lo_db_name_text = "";
        lo_db_user_text = "";
        lo_db_pass_text = "";
        lo_db_event_text = "";
        lo_time_position_text = "";
    }

    public static void initInstance() {
        if (ourInstance == null) {
            // Create the instance
            ourInstance = new Time_Singleton();
        }
    }

    public void readSettings(Context context) {

        SharedPreferences mPref;
        mPref = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        lo_difference = mPref.getLong("difference", 0);
        lo_url_text = mPref.getString("db_url", "localhost");
        lo_db_name_text = mPref.getString("db_name", "d0153979");
        lo_db_user_text = mPref.getString("db_user", "");
        lo_db_pass_text = mPref.getString("db_pass", "");
        lo_db_event_text = mPref.getString("db_event", "");
        lo_time_position_text = mPref.getString("event_time_position", "");

    }

    public void writeSettings(Context context) {

        SharedPreferences mPref = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPref.edit();
        mEditor.putLong("difference", lo_difference);
        mEditor.putString("db_url", lo_url_text);
        mEditor.putString("db_name", lo_db_name_text);
        mEditor.putString("db_user", lo_db_user_text);
        mEditor.putString("db_pass", lo_db_pass_text);
        mEditor.putString("db_event", lo_db_event_text);
        mEditor.putString("event_time_position", lo_time_position_text);

        mEditor.apply();

    }

    public void setSValue(String key, java.lang.String value) {
        switch (key) {
            case "db_url":
                lo_url_text = value;
                break;
            case "db_name":
                lo_db_name_text = value;
                break;
            case "db_user":
                lo_db_user_text = value;
                break;
            case "db_pass":
                lo_db_pass_text = value;
                break;
            case "db_event":
                lo_db_event_text = value;
                break;
            case "event_time_position":
                lo_time_position_text = value;
                break;
        }
    }

    public void setLValue(String key, long value) {
        switch (key) {
            case "difference":
                lo_difference = value;
                break;
        }
    }

    public long getLValue(String key) {
        long result;
        switch (key) {
            case "difference":
                return lo_difference;
        }
        return -1;
    }

    public String getSValue(String key) {
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.getDefault());
        switch (key) {
            case "difference":
                return formatter.format("%d", lo_difference).toString();
            case "db_url":
                return lo_url_text;
            case "db_name":
                return lo_db_name_text;
            case "db_user":
                return lo_db_user_text;
            case "db_pass":
                return lo_db_pass_text;
            case "db_event":
                return lo_db_event_text;
            case "event_time_position":
                return lo_time_position_text;
            default: return "";
        }
    }
}
