package com.riloc.zeiterfassung2;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class settingsPagerAdapter extends FragmentPagerAdapter {

    private final Context context;

    public settingsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        switch (position) {
            case 0:
                return SettingsActivity.GPSFragment.newInstance(position + 1);
            case 1:
                return SettingsActivity.DBFragment.newInstance(position + 1);
            case 2:
                return SettingsActivity.DBResetFragment.newInstance(position + 1);
            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getResources().getString(R.string.settings_time_difference);
            case 1:
                return context.getResources().getString(R.string.settings_database);
            case 2:
                return context.getResources().getString(R.string.settings_reset_local_storage);
        }
        return null;
    }
}
