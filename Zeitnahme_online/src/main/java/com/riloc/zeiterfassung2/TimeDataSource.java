package com.riloc.zeiterfassung2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.File;

class TimeDataSource {
    private static final String TAG = TimeDataSource.class.getSimpleName ( );

    private SQLiteDatabase database;
    private final DatabaseHelper dbHelper;
    private String dataBaseName;
    private String fullDataBaseName;
    private Context mContext;

    public TimeDataSource(Context context) {
        dbHelper = new DatabaseHelper ( context, Time_Singleton.getInstance ( ).getSValue ( "db_event" ) );
        dataBaseName = dbHelper.getDatabaseName ( );
        mContext = context;
    }

    public boolean open() {
        database = dbHelper.getWritableDatabase ( );
        fullDataBaseName = database.getPath ( );
        Log.d ( TAG, "Datenbank-Referenz erhalten. Pfad zur Datenbank: " + database.getPath ( ) );
        return database.isOpen ( );
    }

    public void close() {
        dbHelper.close ( );
        Log.d ( TAG, "Datenbank mit Hilfe des DbHelpers geschlossen." );
    }

    public void clearEventDb() {
        if (database.isOpen ( )) {
            database.delete ( "ergebnisse", null, null );
        }
    }

    public void clearNotAssignedDb() {
        if (database.isOpen ( )) {
            database.delete ( "result_not_assigned", null, null );
        }
    }

    public long deleteFromEventDb(String startnr) {

        long id = database.delete ( "ergebnisse", "start_nr = ?", new String[]{startnr} );
        Log.d ( TAG, "delete: " + id );
        return id;
    }

    public boolean deleteLocaleDb() {

        boolean success = false;
        database.close ( );
        File dir = mContext.getDatabasePath ( dataBaseName );
        File file = new File ( dir, "" );
        File file2 = new File ( dir, "-journal" );
        success = file.delete ( );
        success = (success && file2.delete ( ));
        return success;
    }

    public long deleteFromNotAssignedDb(String startnr) {

        long id = database.delete ( "result_not_assigned", "start_nr = ?", new String[]{startnr} );
        Log.d ( TAG, "delete: " + id );
        return id;
    }

    public String getDatabaseName() {
        return dataBaseName;
    }

    public String getFullDatabaseName() {
        return fullDataBaseName;
    }

    public int getLastInsertId() {
        int result = -1;

        try {
            Cursor cursor = database.query ( "ergebnisse", new String[]{"max(rowid)"}, null,
                    null, null, null, null, null );
            if (cursor.moveToFirst ( )) {
                result = cursor.getInt ( 0 );
            }
            cursor.close ( );
        } catch (SQLiteException ex) {
            Log.d ( TAG, "getLastInsertId: " + ex.getMessage ( ) );
        }

        /*
        get last insert id from backup database
         */
        try {
            Cursor cursor = database.query ( "result_not_assigned", new String[]{"max(rowid)"}, null,
                    null, null, null, null, null );
            if (cursor.moveToFirst ( )) {
                result += cursor.getInt ( 0 );
            }
            cursor.close ( );
        } catch (SQLiteException ex) {
            Log.d ( TAG, "getLastInsertId: " + ex.getMessage ( ) );
        }
        return result;
    }

    public void insert(Time_Table_Entry item) {

        ContentValues values = new ContentValues ( );
        values.put ( "start_nr", item.getStnr ( ) );
        values.put ( Time_Singleton.getInstance ( ).getSValue ( "event_time_position" ), item.getCrossing_line ( ) );
        long id = database.insertWithOnConflict ( "ergebnisse", null, values, SQLiteDatabase.CONFLICT_REPLACE );
        //long id = database.replaceOrThrow("ergebnisse", null, values);
        Log.d ( TAG, "insert: " + id );

    }

    public Cursor select() {
        try {
            return database.query ( "ergebnisse", new String[]{"start_nr", Time_Singleton.getInstance ( ).getSValue ( "event_time_position" )}, "sent = 0",
                    null, null, null, "rowid", null );
        } catch (SQLiteException ex) {
            Log.d ( TAG, "select: " + ex.toString ( ) );
        }
        return null;
    }

    public Cursor selectAll() {
        try {
            String[] columns = new String[]{"start_nr", "zeit1", "zeit2", "zeit3", "zeit4", "zeit5", "zeit6", "zeit7", "sent"};
            return database.query ( "ergebnisse", columns, null, null, null, null, "start_nr", null );
        } catch (SQLiteException ex) {
            Log.d ( TAG, "select: " + ex.toString ( ) );
        }
        return null;
    }

    public Cursor selectNotAssigned() {
        try {
            Cursor cursor = database.query ( "result_not_assigned", new String[]{"start_nr", Time_Singleton.getInstance ( ).getSValue ( "event_time_position" )}, "sent = 0",
                    null, null, null, "rowid", null );
            Log.d ( TAG, "not assigned selected after resume " );
            return cursor;
        } catch (SQLiteException ex) {
            Log.d ( TAG, "select not assigned: " + ex.toString ( ) );
        }
        return null;
    }

    public long update(Time_Table_Entry item) {

        ContentValues values = new ContentValues ( );
        values.put ( "start_nr", item.getStnr ( ) );
        values.put ( Time_Singleton.getInstance ( ).getSValue ( "event_time_position" ), item.getCrossing_line ( ) );
        long id = 0;

        /*
            if item is from database table ergebnisse
         */
        if (item.getErgTable ()) {
            id = database.updateWithOnConflict ( "ergebnisse", values, "rowid = ?", new String[]{String.valueOf ( item.getRowId ( ) )}, SQLiteDatabase.CONFLICT_REPLACE );
            Log.d ( TAG, "update erg: " + id );
        }

        /*
            if item is from database table result_not_assigned
         */
        if (id < 1) {
            id = database.insertWithOnConflict ( "ergebnisse", null, values, SQLiteDatabase.CONFLICT_REPLACE );
            Log.d ( TAG, "insert erg: " + id );
            if (id > 0) {
                item.setRowId ( id );
            }
        }
        return id;
    }

    public long updateSetSent(String startnr) {

        ContentValues values = new ContentValues ( );
        values.put ( "start_nr", startnr );
        values.put ( "sent", "1" );
        long id = database.updateWithOnConflict ( "ergebnisse", values, "start_nr = ?", new String[]{startnr}, SQLiteDatabase.CONFLICT_REPLACE );
        Log.d ( TAG, "update: " + id );
        return id;
    }

    public void insertNotAssigned(Time_Table_Entry item) {

        ContentValues values = new ContentValues ( );
        values.put ( "start_nr", item.getStnr ( ) );
        values.put ( Time_Singleton.getInstance ( ).getSValue ( "event_time_position" ), item.getCrossing_line ( ) );

        long id = database.insertWithOnConflict ( "result_not_assigned", null, values, SQLiteDatabase.CONFLICT_REPLACE );
        if (id > 0) {
            item.setRowId ( id );
            item.setOldStnr ( item.getStnr ( ) );
        }
        Log.d ( TAG, "notAssigned insert: " + id );
    }

    public void updateNotAssigned(Time_Table_Entry item) {

        ContentValues values = new ContentValues ( );
        values.put ( "start_nr", item.getStnr ( ) );
        values.put ( Time_Singleton.getInstance ( ).getSValue ( "event_time_position" ), item.getCrossing_line ( ) );
        long id = database.updateWithOnConflict ( "result_not_assigned", values, "start_nr = ?", new String[]{item.getOldStnr ( )}, SQLiteDatabase.CONFLICT_REPLACE );
        if (id < 1) {
            id = database.insertWithOnConflict ( "result_not_assigned", null, values, SQLiteDatabase.CONFLICT_REPLACE );
            if (id > 0) {
                item.setRowId ( id );
            }
        }
        if (id > 0) {
            item.setOldStnr ( item.getStnr ( ) );
        }
        Log.d ( TAG, "update: " + id );
    }

}
