package com.riloc.zeiterfassung2;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Timing extends Activity {

    private static final String TAG = Activity.class.getSimpleName ( );

    private ArrayList<Time_Table_Entry> liste;
    private Time_Table_Adapter time_list_adapter;
    private int selectedRow = -1;
    private int prevselectedRow = -1;
    private String oldStNr = "";
    private int lastInsertId = 0;
    private boolean databaseIsOpen = false;
    private TimeDataSource dataSource = null;
    private Handler handlerDataSend;
    private Runnable runnableDataSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );

        Context mContext;
        mContext = this.getBaseContext ( );
        setContentView ( R.layout.activity_timing );
        dataSource = new TimeDataSource ( mContext );

        TextView text = findViewById ( R.id.text_position_for_time );
        text.setText ( getResources ( ).getString ( R.string.position, Time_Singleton.getInstance ( ).getSValue ( "event_time_position" ) ) );

        Log.d ( TAG, "onCreate: " + dataSource.getDatabaseName ( ) );

        final ListView mTimeListView = findViewById ( R.id.timeListView );
        mTimeListView.setChoiceMode ( ListView.CHOICE_MODE_SINGLE );
        mTimeListView.setOnItemClickListener ( new AdapterView.OnItemClickListener ( ) {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (selectedRow > -1){
                    View test = mTimeListView.getAdapter ().getView ( selectedRow, mTimeListView.getChildAt ( selectedRow ), mTimeListView );
                    if (!liste.get(selectedRow).getErgTable ())
                        test.setBackgroundColor ( Color.argb ( 100, 255, 100, 100 ) ) ;
                    else
                        test.setBackgroundColor ( Color.argb ( 100, 100, 255, 100 ) ) ;
                }
                selectedRow = i;
                view.setBackgroundColor ( Color.YELLOW );
                oldStNr = liste.get ( i ).getStnr ( );
                Log.d ( TAG, "ListView onItemClick: " + selectedRow );
            }
        } );

        liste = new ArrayList<> ( );
        time_list_adapter = new Time_Table_Adapter ( this, liste );

        mTimeListView.setAdapter ( time_list_adapter );

        Button button1 = findViewById ( R.id.buttonNumber1 );
        button1.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "1" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button2 = findViewById ( R.id.buttonNumber2 );
        button2.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "2" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button3 = findViewById ( R.id.buttonNumber3 );
        button3.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "3" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button4 = findViewById ( R.id.buttonNumber4 );
        button4.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "4" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button5 = findViewById ( R.id.buttonNumber5 );
        button5.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "5" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button6 = findViewById ( R.id.buttonNumber6 );
        button6.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "6" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button7 = findViewById ( R.id.buttonNumber7 );
        button7.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "7" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button8 = findViewById ( R.id.buttonNumber8 );
        button8.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "8" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button9 = findViewById ( R.id.buttonNumber9 );
        button9.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "9" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button button0 = findViewById ( R.id.buttonNumber0 );
        button0.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                setStartNumber ( "0" );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button buttonClear = findViewById ( R.id.buttonNumberclear );
        buttonClear.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                if (selectedRow > -1)
                    liste.get ( selectedRow ).setStnr ( oldStNr );
                mTimeListView.invalidateViews ( );
            }
        } );
        Button buttonOk = findViewById ( R.id.buttonNumberOk );
        buttonOk.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick(View view) {
                if (selectedRow > -1) {
                    long id = dataSource.update ( liste.get ( selectedRow ) );
                    if (id > 0) {
                /*
                    if update affects one row or a new rowid is given
                 */
                        dataSource.deleteFromNotAssignedDb ( liste.get ( selectedRow ).getOldStnr ( ) );
                        liste.get ( selectedRow ).setOldStnr ( liste.get ( selectedRow ).getStnr ( ) );
                        liste.get ( selectedRow ).setErgTable ( true );

                    }
                    if (selectedRow < (liste.size ( ) - 1))
                        ++selectedRow;
                    if (selectedRow > -1)
                        mTimeListView.setSelection ( selectedRow );
                    mTimeListView.invalidateViews ( );
                }
            }
        } );

        /*
        handler controls the sending of the data to sql-server
         */
        handlerDataSend = new Handler ( );
        runnableDataSend = new Runnable ( ) {
            @Override
            public void run() {
                sendSQL ( false );
                /*
                send data to SQL server raw data table every 30 seconds
                 */
                handlerDataSend.postDelayed ( runnableDataSend, 30000 );
                Log.d ( TAG, "runnableDataSend run: done." );
            }
        };
        handlerDataSend.post ( runnableDataSend );
    }

    private void setStartNumber(String number) {
/*
    button with number  clicked replaces existing number starting with an 'x' or add a number
 */
        if (selectedRow > -1)
            if (liste.get ( selectedRow ).getStnr ( ).charAt ( 0 ) == 'x') {
                liste.get ( selectedRow ).setStnr ( number );
            } else {
                liste.get ( selectedRow ).setStnr ( liste.get ( selectedRow ).getStnr ( ) + number );
            }
    }

    @Override
    protected void onResume() {
        super.onResume ( );
        if (dataSource.open ( )) {
            databaseIsOpen = true;
            lastInsertId = dataSource.getLastInsertId ( );

            Cursor cursor = dataSource.selectNotAssigned ( );

            if (cursor.moveToFirst ( )) {
                time_list_adapter.clear ( );
                while (!cursor.isAfterLast ( )) {
                    Log.d ( TAG, "onCreate: " + cursor.getString ( 1 ) );
                    Time_Table_Entry entry = new Time_Table_Entry ( cursor.getString ( 0 ), cursor.getString ( 1 ) );
                    dataSource.updateNotAssigned ( entry );
                    time_list_adapter.add ( entry );
                    /*
                    if (entry.getErgTable ())
                        entry.setBackgroundColor ( Color.argb ( 100, 255, 100, 100 ) ) ;
                    else
                        test.setBackgroundColor ( Color.argb ( 100, 100, 255, 100 ) ) ;
                    View test = mTimeListView.getAdapter ().getView ( selectedRow, mTimeListView.getChildAt ( selectedRow ), mTimeListView );
                    if (liste.get(selectedRow).getErgTable ())
                        test.setBackgroundColor ( Color.argb ( 100, 255, 100, 100 ) ) ;
                    else
                        test.setBackgroundColor ( Color.argb ( 100, 100, 255, 100 ) ) ;*/
                    cursor.moveToNext ( );
                }
            }
            handlerDataSend.post ( runnableDataSend );
        }
    }

    @Override
    protected void onPause() {
        dataSource.close ( );
        databaseIsOpen = false;
        handlerDataSend.removeCallbacks ( runnableDataSend );
        super.onPause ( );
    }

    public void clickSendSQL(View view) {
        sendSQL ( true );
    }

    private void sendSQL(boolean triggeredByClick) {

        String url = Time_Singleton.getInstance ( ).getSValue ( "db_url" );
        url = (url.charAt ( url.length ( ) - 1 ) == '/' ? url.substring ( 0, url.length ( ) - 1 ) : url);

        url += "/set_data.php";
        //String url = "http://192.168.42.100/set_data.php";

        //TimeDataSource dataSource = new TimeDataSource(this);
        //dataSource.open();

        final Cursor cursor = dataSource.select ( );

        Log.d ( TAG, "sendSQL: " + cursor.getCount ( ) );
        if (cursor != null && cursor.getCount ( ) > 0) {
            cursor.moveToFirst ( );

            final JSONArray jsonSend = new JSONArray ( );

            try {

                JSONObject jsonObj = new JSONObject ( );
                jsonObj.put ( "id", 0 );
                jsonObj.put ( "task", "getPositions" );
                jsonSend.put ( jsonObj );
                jsonObj = new JSONObject ( );
                jsonObj.put ( "id", 1 );
                jsonObj.put ( "db_name", Time_Singleton.getInstance ( ).getSValue ( "db_name" ) );
                jsonSend.put ( jsonObj );
                jsonObj = new JSONObject ( );
                jsonObj.put ( "id", 2 );
                jsonObj.put ( "db_user", Time_Singleton.getInstance ( ).getSValue ( "db_user" ) );
                jsonSend.put ( jsonObj );
                jsonObj = new JSONObject ( );
                jsonObj.put ( "id", 3 );
                jsonObj.put ( "db_pass", Time_Singleton.getInstance ( ).getSValue ( "db_pass" ) );
                jsonSend.put ( jsonObj );
                jsonObj = new JSONObject ( );
                jsonObj.put ( "id", 4 );
                jsonObj.put ( "db_event", Time_Singleton.getInstance ( ).getSValue ( "db_event" ) );
                jsonSend.put ( jsonObj );

                while (!cursor.isAfterLast ( )) {
                    jsonObj = new JSONObject ( );
                    jsonObj.put ( "start_nr", cursor.getString ( 0 ) );
                    jsonObj.put ( Time_Singleton.getInstance ( ).getSValue ( "event_time_position" ), cursor.getString ( 1 ) );
                    jsonSend.put ( jsonObj );
                    cursor.moveToNext ( );
                }
            } catch (Exception e) {
                e.printStackTrace ( );
            }

            JsonArrayRequest arrayRequest = new JsonArrayRequest ( Request.Method.POST, url,
                    jsonSend,
                    new Response.Listener<JSONArray> ( ) {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.i ( "LO_VOLLEY_Response: ", response.toString ( ) + " rows inserted" );
                            try {
                                for (int i = 1; i < response.length ( ); ++i) {
                                    dataSource.updateSetSent ( response.getString ( i ) );
                                }
                                String text = ("Erfolg: ").concat ( response.getString ( 0 ) ).concat ( " Eintrag/Einträge gespeichert" );
                                Toast.makeText ( Timing.this, text, Toast.LENGTH_LONG ).show ( );
                            } catch (JSONException e) {
                                Log.i ( "JSON DB sent Events", e.toString ( ) );
                            }
                        }
                    }, new Response.ErrorListener ( ) {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i ( "LOG_VOLLEY_error: ", error.toString ( ) );
                    Toast.makeText ( Timing.this, error.toString ( ), Toast.LENGTH_LONG ).show ( );
                }
            } ) {

            };

            // Add the request to the queue
            Volley.newRequestQueue ( this ).add ( arrayRequest );
        } else if (triggeredByClick) {
            Toast.makeText ( Timing.this, getResources ( ).getString ( R.string.no_record_submitable ), Toast.LENGTH_SHORT ).show ( );
        }
    }

    public void addEntry(View view) {
        lastInsertId++;
        SimpleDateFormat format = new SimpleDateFormat ( "HH:mm:ss.SS", Locale.getDefault ( ) );
        Date system_time = new Date ( System.currentTimeMillis ( ) + Time_Singleton.getInstance ( ).getLValue ( "difference" ) );

        Time_Table_Entry entry = new Time_Table_Entry ( String.format ( Locale.getDefault ( ), "x%d", lastInsertId ), format.format ( system_time ) );
        dataSource.insertNotAssigned ( entry );
        //entry.setRowId ( dataSource.getLastInsertId ( ) );
        time_list_adapter.add ( entry );
    }
}
