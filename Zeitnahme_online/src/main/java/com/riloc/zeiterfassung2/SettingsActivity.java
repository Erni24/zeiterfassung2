package com.riloc.zeiterfassung2;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import android.app.Fragment;

public class SettingsActivity extends FragmentActivity {

    private static final String TAG = Activity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 42;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ViewPager mViewPager = findViewById(R.id.settingsviewPager);
        mViewPager.setAdapter(new settingsPagerAdapter(getSupportFragmentManager(), SettingsActivity.this));

        TabLayout tabLayout = findViewById(R.id.tabLayouter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    /**
     * A fragment containing positioning - system time check.
     */
    public static class GPSFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

        /**
         * Provides the entry point to the Fused Location Provider API.
         */

        private LocationRequest mlocationRequest;
        private LocationCallback mlocationCallback;
        private FusedLocationProviderClient mFusedLocationClient;

        /**
         * Represents a geographical location.
         */
        private String mGPSAbgleich;
        private String mGPSAbgleichlauft;
        private TextView mLatitudeText;
        private TextView mLongitudeText;
        private TextView mPosTimeText;
        private TextView mSystemTimeText;
        private TextView mDiffTimeText;
        private long difference;
        private Button mGPSButton;

        public GPSFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static GPSFragment newInstance(int sectionNumber) {
            GPSFragment fragment = new GPSFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_gps, container, false);

            difference = 0;
            // strings used later in the app
            mGPSAbgleich = getResources().getString(R.string.gps_abgleich);
            mGPSAbgleichlauft = getResources().getString(R.string.gps_abgleich_running);

            //set labels
            ((TextView) rootView.findViewById(R.id.label_latitude)).setText(getResources().getString(R.string.latitude));
            ((TextView) rootView.findViewById(R.id.label_longitude)).setText(getResources().getString(R.string.longitude));
            ((TextView) rootView.findViewById(R.id.label_positioning_time)).setText(getResources().getString(R.string.positioning_time));
            ((TextView) rootView.findViewById(R.id.label_system_time)).setText(getResources().getString(R.string.system_time));
            ((TextView) rootView.findViewById(R.id.label_time_diff)).setText(getResources().getString(R.string.time_diff));

            mLatitudeText = rootView.findViewById((R.id.latitude_text));
            mLongitudeText = rootView.findViewById((R.id.longitude_text));
            mPosTimeText = rootView.findViewById((R.id.gps_time_text));
            mSystemTimeText = rootView.findViewById((R.id.system_time_text));
            mDiffTimeText = rootView.findViewById((R.id.diff_time_text));
            mGPSButton = rootView.findViewById((R.id.gps_button));

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(rootView.getContext());

            mlocationRequest = new LocationRequest();

            mlocationRequest.setInterval(7500); //use a value fo about 10 to 15s for a real app

            mlocationRequest.setFastestInterval(5000);

            mlocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            mlocationCallback = new LocationCallback() {

                @Override

                public void onLocationResult(LocationResult locationResult) {

                    super.onLocationResult(locationResult);

                    for (Location location : locationResult.getLocations()) {

                        //Update UI with location data

                        if (location != null) {

                            mLatitudeText.setText(String.valueOf(location.getLatitude()));

                            mLongitudeText.setText(String.valueOf(location.getLongitude()));

                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.getDefault());

                            Date gps_time = new Date(location.getTime());
                            Date system_time = new Date();
                            mPosTimeText.setText(format.format(gps_time));
                            mSystemTimeText.setText(format.format(system_time));
                            difference = gps_time.getTime() - system_time.getTime();
                            mDiffTimeText.setText(String.format(Locale.getDefault(), "%d", difference));

                            mGPSButton.setText(mGPSAbgleich);
                            mGPSButton.setEnabled(true);
                        }

                    }

                }

            };

            mGPSButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!checkPermissions()) {
                        requestPermissions();
                    } else {
                        mGPSButton.setText(mGPSAbgleichlauft);
                        mGPSButton.setEnabled(false);
                            if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            mFusedLocationClient.requestLocationUpdates(mlocationRequest, mlocationCallback, null);
                            //getLastLocation();
                    }
                }
            });
            return rootView;
        }

        /**
         * Return the current state of the permissions needed.
         */
        private boolean checkPermissions() {
            int permissionState = ActivityCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION);
            return permissionState == PackageManager.PERMISSION_GRANTED;
        }

        private void requestPermissions() {
            boolean shouldProvideRationale =
                    ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION);

            // Provide an additional rationale to the user. This would happen if the user denied the
            // request previously, but didn't check the "Don't ask again" checkbox.
            if (shouldProvideRationale) {
                Log.i("Positioning Request:", "Displaying permission rationale to provide additional context.");
/*
                showSnackbar(R.string.permission_rationale, android.R.string.ok,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Request permission
                                startLocationPermissionRequest();
                            }
                        });*/

            } else {
                Log.i("Positioning Request:", "Requesting permission");
                // Request permission. It's possible this can be auto answered if device policy
                // sets the permission in a given state or the user denied the permission
                // previously and checked "Never ask again".
                startLocationPermissionRequest();
            }
        }

        private void startLocationPermissionRequest() {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }

        @Override
        public void onResume() {
            super.onResume();
            mDiffTimeText.setText(Time_Singleton.getInstance().getSValue("difference"));
        }


        @Override
        public void onPause() {
            super.onPause();
            long lo_long;
            try{
                lo_long = Long.parseLong(mDiffTimeText.getText().toString());
            } catch (NumberFormatException e){
                lo_long = 0;
            }
            Time_Singleton.getInstance().setLValue("difference", lo_long);
        }
    }

    /**
     * A fragment containing database connection settings.
     */
    public static class DBFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public DBFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static DBFragment newInstance(int sectionNumber) {
            DBFragment fragment = new DBFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_db, container, false);

            final EditText db_url_text = rootView.findViewById(R.id.editText_db_address);
            final EditText db_name_text = rootView.findViewById(R.id.editText_db_name);
            final EditText db_user_text = rootView.findViewById(R.id.editText_db_user);
            final EditText db_pass_text = rootView.findViewById(R.id.editText_db_pass);

            Button button_events = rootView.findViewById(R.id.check_db_button);
            button_events.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // open connection to server and hand over all information in JSON

                    String url = db_url_text.getText().toString();
                    url = (url.charAt(url.length() - 1) == '/' ? url.substring(0, url.length() - 1) : url);
                    url += "/get_data.php";

                    final JSONArray jsonSend = new JSONArray();

                    try {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("id", 0);
                        jsonObj.put("task", "getEvents");
                        jsonSend.put(jsonObj);
                        jsonObj = new JSONObject();
                        jsonObj.put("id", 1);
                        jsonObj.put("db_name", db_name_text.getText().toString());
                        jsonSend.put(jsonObj);
                        jsonObj = new JSONObject();
                        jsonObj.put("id", 2);
                        jsonObj.put("db_user", db_user_text.getText().toString());
                        jsonSend.put(jsonObj);
                        jsonObj = new JSONObject();
                        jsonObj.put("id", 3);
                        jsonObj.put("db_pass", db_pass_text.getText().toString());
                        jsonSend.put(jsonObj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.POST, url,
                            jsonSend,
                            new Response.Listener<JSONArray>() {
                                @Override
                                public void onResponse(JSONArray response) {
                                    Log.i("LO_VOLLEY", response.toString());
                                    Spinner spinn = rootView.findViewById(R.id.spinner_event_name);

                                    List<String> positions = new ArrayList<>();
                                    try {

                                        for (int i = 1; i < response.length(); i++) {
                                            JSONObject jsonobject = response.getJSONObject(i);
                                            positions.add(jsonobject.getString("prefix"));
                                        }
                                    } catch (JSONException e) {
                                        Log.i("JSON DB Events", e.toString());
                                    }
                                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(rootView.getContext(), android.R.layout.simple_spinner_item, positions);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    // attaching data adapter to spinner
                                    spinn.setAdapter(dataAdapter);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("LOG_VOLLEY", error.toString());
                            Toast.makeText(rootView.getContext(), error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {

                    };

                    // Add the request to the queue
                    Volley.newRequestQueue(rootView.getContext()).add(arrayRequest);

                }
            });

            Button button_position = rootView.findViewById(R.id.check_event_button);
            button_position.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*
                        open connection to server and look in the selected event for table 'event_abrev'_ergebnisse
                        and take all column names except 'start_nr' and put them all into the spinner
                     */
                    Spinner db_event_spinner = rootView.findViewById(R.id.spinner_event_name);

                    String url = db_url_text.getText().toString();
                    url = (url.charAt(url.length() - 1) == '/' ? url.substring(0, url.length() - 1) : url);

                    url += "/get_data.php";

                    final JSONArray jsonSend = new JSONArray();

                    try {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("id", 0);
                        jsonObj.put("task", "getPositions");
                        jsonSend.put(jsonObj);
                        jsonObj = new JSONObject();
                        jsonObj.put("id", 1);
                        jsonObj.put("db_name", db_name_text.getText().toString());
                        jsonSend.put(jsonObj);
                        jsonObj = new JSONObject();
                        jsonObj.put("id", 2);
                        jsonObj.put("db_user", db_user_text.getText().toString());
                        jsonSend.put(jsonObj);
                        jsonObj = new JSONObject();
                        jsonObj.put("id", 3);
                        jsonObj.put("db_pass", db_pass_text.getText().toString());
                        jsonSend.put(jsonObj);
                        jsonObj = new JSONObject();
                        jsonObj.put("id", 4);
                        jsonObj.put("db_event", db_event_spinner.getSelectedItem().toString());
                        jsonSend.put(jsonObj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.POST, url,
                            jsonSend,
                            new Response.Listener<JSONArray>() {
                                @Override
                                public void onResponse(JSONArray response) {
                                    Log.i("LO_VOLLEY", response.toString());
                                    Spinner spinn = rootView.findViewById(R.id.spinner_time_position);

                                    List<String> positions = new ArrayList<>();
                                    try {
                                        //first field in table 'ergebnisse' is 'startnr', that's why start with second entry
                                        for (int i = 1; i < response.length(); i++) {
                                            JSONObject jsonobject = response.getJSONObject(i);
                                            positions.add(jsonobject.getString("Field"));
                                        }
                                    } catch (JSONException e) {
                                        Log.i("JSON DB Position", e.toString());
                                    }
                                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(rootView.getContext(), android.R.layout.simple_spinner_item, positions);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    // attaching data adapter to spinner
                                    spinn.setAdapter(dataAdapter);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("LOG_VOLLEY", error.toString());
                        }
                    }) {

                    };

                    // Add the request to the queue
                    Volley.newRequestQueue(rootView.getContext()).add(arrayRequest);

                }
            });
            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();

            EditText url_text = this.getView().findViewById(R.id.editText_db_address);
            url_text.setText(Time_Singleton.getInstance().getSValue("db_url"));

            EditText db_name = this.getView().findViewById(R.id.editText_db_name);
            db_name.setText(Time_Singleton.getInstance().getSValue("db_name"));

            EditText db_user = this.getView().findViewById(R.id.editText_db_user);
            db_user.setText(Time_Singleton.getInstance().getSValue("db_user"));

            EditText db_pass = this.getView().findViewById(R.id.editText_db_pass);
            db_pass.setText(Time_Singleton.getInstance().getSValue("db_pass"));

            Spinner db_event_spinner = this.getView().findViewById(R.id.spinner_event_name);
            List<String> events = new ArrayList<>();
            events.add(Time_Singleton.getInstance().getSValue("db_event"));
            ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, events);
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            db_event_spinner.setAdapter(dataAdapter1);

            Spinner time_position_spinner = this.getView().findViewById(R.id.spinner_time_position);
            List<String> positions = new ArrayList<>();
            positions.add(Time_Singleton.getInstance().getSValue("event_time_position"));
            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, positions);
            dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            time_position_spinner.setAdapter(dataAdapter2);

        }

        @Override
        public void onPause() {
            super.onPause();

            EditText url_text = this.getView().findViewById(R.id.editText_db_address);
            Time_Singleton.getInstance().setSValue("db_url", url_text.getText().toString());

            EditText db_name = this.getView().findViewById(R.id.editText_db_name);
            Time_Singleton.getInstance().setSValue("db_name", db_name.getText().toString());

            EditText db_user = this.getView().findViewById(R.id.editText_db_user);
            Time_Singleton.getInstance().setSValue("db_user", db_user.getText().toString());

            EditText db_pass = this.getView().findViewById(R.id.editText_db_pass);
            Time_Singleton.getInstance().setSValue("db_pass", db_pass.getText().toString());

            Spinner db_event_spinner = this.getView().findViewById(R.id.spinner_event_name);
            Time_Singleton.getInstance().setSValue("db_event", db_event_spinner.getSelectedItem().toString());

            Spinner time_position_spinner = this.getView().findViewById(R.id.spinner_time_position);
            Time_Singleton.getInstance().setSValue("event_time_position", time_position_spinner.getSelectedItem().toString());

        }

        public void onStop (){
            onPause();
            super.onStop();
        }
    }

    /**
     * A fragment containing reset for local sqlite databases.
     */
    public static class DBResetFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public static DBResetFragment newInstance(int sectionNumber){
            DBResetFragment fragment = new DBResetFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_sqlite, container, false);

            Button buttonSendLocaleDb = rootView.findViewById(R.id.button_send_current_locale_db);
            buttonSendLocaleDb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendByMail(rootView);
                }
            });

            /*
            first deletes all records in 'result_not_assigned', then deletes all records in 'ergebnissse' database
             */
            Button buttonClearLocaleDb = rootView.findViewById(R.id.button_clear_current_locale_db);
            buttonClearLocaleDb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(getResources().getString(R.string.settings_message_clear_current_database))
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    TimeDataSource dataSource = new TimeDataSource(getContext());
                                    dataSource.open();
                                    dataSource.clearNotAssignedDb();
                                    dataSource.clearEventDb();
                                    dataSource.close();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            /*
            deletes the database file
             */
            Button buttonDeleteCurrentLocaleDb = rootView.findViewById(R.id.button_delete_current_locale_db);
            buttonDeleteCurrentLocaleDb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(getResources().getString(R.string.settings_message_delete_current_database))
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    TimeDataSource dataSource = new TimeDataSource(getContext());
                                    dataSource.open();
                                    dataSource.deleteLocaleDb();

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            Button buttonDeleteAllLocaleDb = rootView.findViewById(R.id.button_delete_all_local_db);
            buttonDeleteAllLocaleDb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(getResources().getString(R.string.settings_message_delete_all_database))
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            return rootView;
        }

        private void sendByMail(View view){
            Log.i("Send email", "");
            String[] TO = {
                    "dietmar.czekay@riloc.de"
            };
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            //emailIntent.setData(Uri.parse("mailto:dietmar.czekay@riloc.de"));
            emailIntent.setType("plain/text");

            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");

            TimeDataSource dataSource = new TimeDataSource(getContext());
            dataSource.open();
            Cursor cursor = dataSource.selectAll();

            StringBuilder content = new StringBuilder();

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        content.append(cursor.getString(0)).append(";").append(cursor.getString(1)).append(";").append(cursor.getString(2)).append(";")
                                .append(cursor.getString(3)).append(";").append(cursor.getString(4)).append(";").append(cursor.getString(5)).append(";")
                                .append(cursor.getString(6)).append(";").append(cursor.getString(7)).append(";").append(cursor.getString(8)).append(System.getProperty("line.separator"));
                    } while (cursor.moveToNext());

                }
                cursor.close();
            }

            emailIntent.putExtra(Intent.EXTRA_TEXT, content.toString());
            File file = null;
            file = copyFile(dataSource.getDatabaseName(), Time_Singleton.getInstance().getSValue("db_event"));

            if (file != null) {
                Uri fileURI = FileProvider.getUriForFile(getContext(), "com.riloc.fileprovider", file);
                emailIntent.putExtra(Intent.EXTRA_STREAM, fileURI);
            }

            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));

                Log.i("Finished sending email...", "");
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(view.getContext(), ex.toString(), Toast.LENGTH_SHORT).show();
            }

            if (file != null) {
               boolean success = file.delete();
               if (success){
                   Toast.makeText(getContext(), "Message sent and temporary file deleted.", Toast.LENGTH_SHORT).show();
               }
            }

            dataSource.close();
        }

        private File copyFile(String database, String event){

            try {
                if (
                        ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Explain to the user why we need to read the contacts
                        Toast.makeText(getContext(), getResources().getString(R.string.settings_explain_read_permission), Toast.LENGTH_LONG).show();
                    }

                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                    // app-defined int constant that should be quite unique
                }

                //File dst = new File(Environment.getExternalStorageDirectory().getPath() + File.separator);
                File dst = new File(getContext().getFilesDir(), "docs");
                //if folder does not exist
                if (!dst.exists()) {
                    if (!dst.mkdir()) {
                        return null;
                    }
                }

                File src = new File(getContext().getDatabasePath(database), "");

                File dstfile = new File(dst.getPath()+File.separator + database+"." + event + ".sqlite");

                FileChannel inChannel = new FileInputStream(src).getChannel();
                FileChannel outChannel = new FileOutputStream(dstfile).getChannel();
                outChannel.transferFrom(inChannel, 0, inChannel.size());
                inChannel.close();
                outChannel.close();

                return dstfile;

            }catch(Exception ex){
                Log.d(TAG, "copyFile: " + ex.toString());
            }
            return null;
        }
    }
}