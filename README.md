Die Zeiterfassung soll mit normalen Smartphones erfolgen.
Dabei wird initial der Streckenstandort festgelegt und die Abweichung der GPS-Zeit zur Systemzeit.
Gespeichert werden auf dem Server die Abweichung und dann fortlaufend nur die Startnummer und die Smartphone-Zeit.